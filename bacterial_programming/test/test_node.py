from nose import tools as nose_tools
from bacterial_programming.src.ability import Ability
from bacterial_programming.src.node import Node


def test_init():

    a_variable = Ability(function='x', function_result_type=float)
    a_1 = Ability(function=lambda: 1, function_result_type=float)
    a_2 = Ability(function=lambda: 2, function_result_type=float, sign="overwritten by 2")
    a_sum = Ability(function=lambda x, y: x+y, function_result_type=float,
                    arg_types_list=[float, float], sign="+")

    node_variable = Node(ability=a_variable)
    node_1 = Node(ability=a_1)
    node_2 = Node(ability=a_2)
    node_sum = Node(ability=a_sum, children=[node_variable, node_1])

    nose_tools.assert_equal(node_sum.eval({'x': 9}), 10)

    nose_tools.assert_equal(node_variable.state, None)
    nose_tools.assert_equal(node_1.state, 1)
    nose_tools.assert_equal(node_2.state, 2)
    nose_tools.assert_equal(node_sum.state, None)

    nose_tools.assert_equal(node_variable.sign, "x")
    nose_tools.assert_equal(node_1.sign, "1")
    nose_tools.assert_equal(node_2.sign, "overwritten by 2")
    nose_tools.assert_equal(node_sum.sign, "+")


def test_eval():
    a_variable = Ability(function='x', function_result_type=float)
    a_1 = Ability(function=lambda: 1, function_result_type=float)
    a_sum = Ability(function=lambda x, y: x + y, function_result_type=float,
                    arg_types_list=[float, float], sign="+")

    node_variable = Node(ability=a_variable)
    node_1 = Node(ability=a_1)
    node_sum = Node(ability=a_sum, children=[node_variable, node_1])

    nose_tools.assert_equal(node_sum.eval({"x": 1}), 2)

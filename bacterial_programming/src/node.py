class Node:
    """Nodes of a Bacterial Programming Tree"""

    def __init__(self, ability, parent=None, children=None, state=None):
        self.ability = ability
        self.parent = parent
        self.children = children
        if state:
            self.state = state
        else:
            self.state = state if (self.ability.arg_types_list or self.ability.is_variable) else self.ability.function()
        self.sign = ability.function if ability.is_variable else ability.sign or str(self.state)

    def eval(self, variables=None):
        if self.state is not None:
            return self.state
        else:
            if self.ability.is_variable:
                return variables[self.ability.function]
            else:
                my_args = [node.eval(variables=variables) for node in self.children]
                return self.ability.function(*my_args)

    def add_child(self, child_node):
        if not self.children:
            self.children = []
        self.children.append(child_node)
        child_node.parent = self
        return child_node

    def become_copy_of(self, node):
        if node.children:
            self.__init__(ability=node.ability, parent=node.parent,
                          children=node.children.copy(), state=node.state)
        else:
            self.__init__(ability=node.ability, parent=node.parent, state=node.state)

    def clone(self):
        if self.children:
            return Node(ability=self.ability, parent=self.parent,
                        children=self.children.copy(), state=self.state)
        else:
            return Node(ability=self.ability, parent=self.parent, state=self.state)

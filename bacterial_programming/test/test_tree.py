from nose import tools as nose_tools
from bacterial_programming.src.ability import Ability
from bacterial_programming.src.node import Node
from bacterial_programming.src.tree import Tree


def test_init():
    a_variable = Ability(function='x', function_result_type=float)
    a_1 = Ability(function=lambda: 1, function_result_type=float)
    a_sum = Ability(function=lambda x, y: x + y, function_result_type=float,
                    arg_types_list=[float, float], sign="+")

    abilities = [a_variable, a_1, a_sum]

    for _ in range(50):
        Tree(abilities=abilities)


def test_node_count():
    a_variable = Ability(function='x', function_result_type=float)
    a_1 = Ability(function=lambda: 1, function_result_type=float)
    a_sum = Ability(function=lambda x, y: x + y, function_result_type=float,
                    arg_types_list=[float, float], sign="+")

    node_variable = Node(ability=a_variable)
    node_1 = Node(ability=a_1)
    node_sum = Node(ability=a_sum, children=[node_variable, node_1])
    node_variable.parent = node_sum
    node_1.parent = node_sum

    tree = Tree(abilities=[a_variable, a_sum, a_sum])

    tree.root = node_sum
    nose_tools.assert_equal(3, tree.node_count())
    nose_tools.assert_equal(1, tree.node_count_from_node(node_1))


def test_eval():
    a_variable = Ability(function='x', function_result_type=float)
    a_1 = Ability(function=lambda: 1, function_result_type=float)
    a_sum = Ability(function=lambda x, y: x + y, function_result_type=float,
                    arg_types_list=[float, float], sign="+")

    node_variable = Node(ability=a_variable)
    node_1 = Node(ability=a_1)
    node_sum = Node(ability=a_sum, children=[node_variable, node_1])
    node_variable.parent = node_sum
    node_1.parent = node_sum

    tree = Tree(abilities=[a_variable, a_sum, a_sum])

    tree.root = node_sum
    nose_tools.assert_equal(3, tree.node_count())
    nose_tools.assert_equal(1, tree.node_count_from_node(node_1))

    nose_tools.assert_equal(tree.eval({"x": 1}), 2)


def test_bacterial_mutate():
    a_variable = Ability(function='x', function_result_type=float)
    a_1 = Ability(function=lambda: 1, function_result_type=float)
    a_sum = Ability(function=lambda x, y: x + y, function_result_type=float,
                    arg_types_list=[float, float], sign="+")
    a_multiply = Ability(function=lambda x, y: x * y, function_result_type=float,
                         arg_types_list=[float, float], sign="*")

    tree = Tree(abilities=[a_variable, a_1, a_sum, a_multiply],
                only_functionals_below_count=1,
                only_terminals_above_count=5)

    def fitness_function(t):
        x = [-1, 0, 1, 5, 7]

        fitness = 0
        for i in x:
            fitness = fitness + abs(t.eval({'x': i}) - (i * i + i + 1))
        fitness = fitness + t.node_count() - 7
        return fitness

    for _ in range(20):
        previous_score = fitness_function(tree)
        tree.bacterial_mutate(fitness_function=fitness_function)
        new_score = fitness_function(tree)
        nose_tools.assert_greater_equal(previous_score, new_score)


def test_infect_tree():
    a_variable = Ability(function='x', function_result_type=float)
    a_1 = Ability(function=lambda: 1, function_result_type=float)
    a_sum = Ability(function=lambda x, y: x + y, function_result_type=float,
                    arg_types_list=[float, float], sign="+")
    a_multiply = Ability(function=lambda x, y: x * y, function_result_type=float,
                         arg_types_list=[float, float], sign="*")

    tree = Tree(abilities=[a_variable, a_1, a_sum, a_multiply],
                only_functionals_below_count=1,
                only_terminals_above_count=2)

    tree2 = Tree(abilities=[a_variable, a_1, a_sum, a_multiply],
                 only_functionals_below_count=1,
                 only_terminals_above_count=1)

    def fitness_function(t):
        x = [-1, 0, 1, 5, 7]

        fitness = 0
        for i in x:
            fitness = fitness + abs(t.eval({'x': i}) - (i * i + i + 1))
        fitness = fitness + t.node_count() - 7
        return fitness

    for _ in range(50):
        tree.infect_tree(tree2)
        tree2.infect_tree(tree)

    nose_tools.assert_greater(fitness_function(tree), -1)
    nose_tools.assert_greater(fitness_function(tree2), -1)

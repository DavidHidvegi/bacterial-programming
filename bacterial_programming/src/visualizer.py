# https://anytree.readthedocs.io/en/latest/
from anytree import Node, RenderTree
from anytree.exporter import UniqueDotExporter


class TreeVisualizer:

    def __init__(self, bp_tree):
        self._bp_tree = bp_tree
        self._bp_root_node = bp_tree.root
        self._draw_root_node = Node(self._bp_root_node.sign)
        self._process_tree()

    def _process_tree(self):

        def scan_tree_recursively(bp_node, draw_node):
            if not bp_node.ability.arg_types_list:
                return
            for child in bp_node.children:
                ch = Node(child.sign, parent=draw_node)
                scan_tree_recursively(child, ch)

        scan_tree_recursively(self._bp_root_node, self._draw_root_node)

    def draw_in_console(self):
        for pre, fill, node in RenderTree(self._draw_root_node):
            print("%s%s" % (pre, node.name))

    def export_as_image(self, image_path='tree.png'):
        UniqueDotExporter(self._draw_root_node).to_picture(image_path)

import random

from bacterial_programming.src.ability import Ability
from bacterial_programming.src.bp import BacterialProgramming
from bacterial_programming.src.visualizer import TreeVisualizer


def test_visualizer_init():
    abilities = [
        Ability(function='x', function_result_type=float),
        Ability(function=lambda: random.randint(-10, 10), function_result_type=float),
        Ability(function=lambda x, y: x + y, function_result_type=float,
                arg_types_list=[float, float], sign="+"),
        Ability(function=lambda x, y: x * y, function_result_type=float,
                arg_types_list=[float, float], sign="*"),
    ]

    def fitness_function(t):
        x_a = [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5]

        def fx(x):
            return x * x + x + 5

        fitness = 0
        for xi in x_a:
            fitness = fitness + abs(t.eval({'x': xi}) - fx(xi))
        fitness = fitness + t.node_count()
        return fitness

    bp = BacterialProgramming(abilities=abilities,
                              fitness_function=fitness_function,
                              population_size=4,
                              only_functionals_below_count=1,
                              only_terminals_above_count=7)

    bp.learn_until(gen_count=4, best_fitness_value=0)

    TreeVisualizer(bp.best_tree())

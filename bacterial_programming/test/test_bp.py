import nose
import random

from nose import tools as nose_tools
from bacterial_programming.src.bp import BacterialProgramming
from bacterial_programming.src.ability import Ability


def test_learn_until():
    a_variable = Ability(function='x', function_result_type=float)
    a_1 = Ability(function=lambda: 1, function_result_type=float, sign="1")
    a_sum = Ability(function=lambda x, y: x + y, function_result_type=float,
                    arg_types_list=[float, float], sign="+")
    a_multiply = Ability(function=lambda x, y: x * y, function_result_type=float,
                         arg_types_list=[float, float], sign="*")

    def fitness_function(tree):
        x = [-1, 0, 1, 5, 7]

        fitness = 0
        for i in x:
            fitness = fitness + abs(tree.eval({'x': i}) - (i * i + i + 1))
        fitness = fitness + tree.node_count() - 7
        return fitness

    bp = BacterialProgramming(abilities=[a_variable, a_sum, a_1, a_multiply],
                              fitness_function=fitness_function,
                              population_size=7,
                              only_functionals_below_count=0,
                              only_terminals_above_count=5)

    bp.learn_until(gen_count=100, best_fitness_value=0)
    nose_tools.assert_equal(0, fitness_function(bp.best_tree()))


def test_sample_problem():

    a_variable = Ability(function='x', function_result_type=float)
    a_1 = Ability(function=lambda: 1, function_result_type=float)
    a_sum = Ability(function=lambda x, y: x + y, function_result_type=float,
                    arg_types_list=[float, float], sign="+")
    a_multiply = Ability(function=lambda x, y: x * y, function_result_type=float,
                         arg_types_list=[float, float], sign="*")

    def fitness_function(tree):
        x = [-1, 0, 1, 5, 7]

        fitness = 0
        for i in x:
            fitness = fitness + abs(tree.eval({'x': i}) - (i * i + i + 1))
        fitness = fitness + tree.node_count() - 7
        return fitness

    bp = BacterialProgramming(abilities=[a_variable, a_sum, a_1, a_multiply],
                              fitness_function=fitness_function,
                              population_size=4,
                              only_functionals_below_count=1,
                              only_terminals_above_count=3)

    previous_fitness = fitness_function(bp.best_tree())
    for generation in range(10):
        bp.next_gen()
        new_fitness = fitness_function(bp.best_tree())
        nose_tools.assert_greater_equal(previous_fitness, new_fitness)
        previous_fitness = new_fitness


def test_sample_problem_find_polinom_function():
    abilities = [
        Ability(function='x', function_result_type=float),
        Ability(function=lambda: random.randint(-10, 10), function_result_type=float),
        Ability(function=lambda x, y: x + y, function_result_type=float,
                arg_types_list=[float, float], sign="+"),
        Ability(function=lambda x, y: x * y, function_result_type=float,
                arg_types_list=[float, float], sign="*"),
    ]

    def fitness_function(t):
        x_a = [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5]

        def fx(x):
            return x * x * x + x * x + 5

        fitness = 0
        for xi in x_a:
            fitness = fitness + abs(t.eval({'x': xi}) - fx(xi))
        fitness = fitness + t.node_count()
        return fitness

    bp = BacterialProgramming(abilities=abilities,
                              fitness_function=fitness_function,
                              population_size=4,
                              only_functionals_below_count=1,
                              only_terminals_above_count=7)

    bp.learn_until(gen_count=4, best_fitness_value=0)


def test_sample_problem_with_random_terminal():

    a_variable = Ability(function='x', function_result_type=float)
    a_1 = Ability(function=lambda: random.randint(-10, 10), function_result_type=float)
    a_sum = Ability(function=lambda x, y: x + y, function_result_type=float,
                    arg_types_list=[float, float], sign="+")
    a_multiply = Ability(function=lambda x, y: x * y, function_result_type=float,
                         arg_types_list=[float, float], sign="*")

    def fitness_function(tree):
        x = [-1, 0, 1, 5, 7]

        fitness = 0
        for i in x:
            fitness = fitness + abs(tree.eval({'x': i}) - (i * i + i + 1))
        fitness = fitness + tree.node_count() - 7
        return fitness

    bp = BacterialProgramming(abilities=[a_variable, a_sum, a_1, a_multiply],
                              fitness_function=fitness_function,
                              population_size=4,
                              only_functionals_below_count=1,
                              only_terminals_above_count=3)

    previous_fitness = fitness_function(bp.best_tree())
    for generation in range(10):
        bp.next_gen()
        new_fitness = fitness_function(bp.best_tree())
        nose_tools.assert_greater_equal(previous_fitness, new_fitness)
        previous_fitness = new_fitness


def test_ability_with_zero_state():

    a_variable = Ability(function='x', function_result_type=float)
    a_1 = Ability(function=lambda: 0, function_result_type=float)
    a_sum = Ability(function=lambda x, y: x + y, function_result_type=float,
                    arg_types_list=[float, float], sign="+")
    a_multiply = Ability(function=lambda x, y: x * y, function_result_type=float,
                         arg_types_list=[float, float], sign="*")

    def fitness_function(tree):
        x = [-1, 0, 1, 5, 7]

        fitness = 0
        for i in x:
            fitness = fitness + abs(tree.eval({'x': i}) - (i * i + i + 1))
        fitness = fitness + tree.node_count() - 7
        return fitness

    bp = BacterialProgramming(abilities=[a_variable, a_sum, a_1, a_multiply],
                              fitness_function=fitness_function,
                              population_size=4,
                              only_functionals_below_count=1,
                              only_terminals_above_count=3)

    for generation in range(3):
        bp.next_gen()


if __name__ == '__main__':
    nose.run()

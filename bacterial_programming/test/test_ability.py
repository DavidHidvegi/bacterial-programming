from nose import tools as nose_tools
from bacterial_programming.src.ability import Ability


def test_validate_self():

    Ability(function='x', function_result_type=float)
    Ability(function=lambda: 1, function_result_type=float)
    Ability(function=lambda: "text", function_result_type="string")
    Ability(function=lambda: ["text", "test"], function_result_type="string_array")

    Ability(function=lambda x, y: x+y, function_result_type=float,
            arg_types_list=[float, float], sign="+")

    def multiply(a, b):
        return a * b

    Ability(function=multiply, function_result_type=float,
            arg_types_list=[float, float], sign="*")

    with nose_tools.assert_raises(Exception) as error:
        Ability(function=lambda x: 1, function_result_type=float)
    nose_tools.assert_equals(error.exception.args[0], Ability.EXCEPTION_FUNCTION_EMPTY_ARGS)

    with nose_tools.assert_raises(Exception) as error:
        Ability(function=lambda x: 1, function_result_type=float, arg_types_list=[float, float])
    nose_tools.assert_equals(error.exception.args[0], Ability.EXCEPTION_FUNCTION_ARGS_COUNT_MISMATCH)

    with nose_tools.assert_raises(Exception) as error:
        Ability(function=1, function_result_type=float)
    nose_tools.assert_equals(error.exception.args[0], Ability.EXCEPTION_TYPE_ERROR)

    with nose_tools.assert_raises(Exception) as error:
        Ability(function=lambda x, y: x + y, function_result_type=float,
                arg_types_list=[float, float])
    nose_tools.assert_equals(error.exception.args[0], Ability.EXCEPTION_EMPTY_SIGN)

    with nose_tools.assert_raises(Exception) as error:
        Ability(function="x", function_result_type=float, arg_types_list=[float, float])
    nose_tools.assert_equals(error.exception.args[0], Ability.EXCEPTION_VARIABLE_WITH_ARGS)

    nose_tools.assert_true(Ability(function='x', function_result_type=float).is_variable)
    nose_tools.assert_false(Ability(function=lambda: 1, function_result_type=float, sign="1").is_variable)

# Welcome!

This is a repository for the Bacterial Programming Python package I made for my BSc thesis project.

Find out more about Bacterial Programming here: [Genetic and Bacterial Programming for B-Spline NeuralNetworks Design (János Botzheim, Cristiano Cabrita, László T. Kóczy, and Antonio E. Ruano)](https://pdfs.semanticscholar.org/ec15/318399453dc0e3192cc4cef80dd574eb5a85.pdf).

You can find my thesis in hungarian [here](https://www.overleaf.com/read/gxmmtpkgwczy) and source code of the Flutter App I created [here](https://bit.ly/bmememo).
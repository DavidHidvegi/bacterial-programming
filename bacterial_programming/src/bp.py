import random
import math

from bacterial_programming.src.tree import Tree


class BacterialProgramming:
    """The main class that the user can interact with"""

    def __init__(self, abilities, fitness_function, clone_count=None, infection_count=None,
                 population_size=None, only_functionals_below_count=None,
                 only_terminals_above_count=None):
        self.abilities = abilities
        self.fitness_function = fitness_function
        self.population_size = population_size or 2 * len(abilities)
        self.clone_count = clone_count or len(abilities)
        self.infection_count = infection_count or int(len(abilities) / 2)
        self.population = []
        self.gen_count = 0
        self.only_functionals_below_count = only_functionals_below_count or -1
        self.only_terminals_above_count = only_terminals_above_count or 2*len(abilities)
        self._init()

    def _init(self):
        for _ in range(self.population_size):
            self.population.append(Tree(abilities=self.abilities,
                                        only_terminals_above_count=self.only_terminals_above_count,
                                        only_functionals_below_count=self.only_functionals_below_count))

    def next_gen(self):

        self.bacterial_mutate_trees()
        self.gene_transfer()

        self.gen_count = self.gen_count + 1

    def bacterial_mutate_trees(self):
        for tree in self.population:
            tree.bacterial_mutate(fitness_function=self.fitness_function, clone_count=self.clone_count)

    def gene_transfer(self):
        for _ in range(self.infection_count):
            list_by_fitness = sorted(self.population, key=lambda t: self.fitness_function(t), reverse=False)
            half_len_list = int(len(list_by_fitness) / 2)
            superior_trees = list_by_fitness[:half_len_list]
            inferior_trees = list_by_fitness[half_len_list:]

            superior_trees[random.randint(0, len(superior_trees)-1)].infect_tree(
                inferior_trees[random.randint(0, len(inferior_trees)-1)])

    def learn_until(self, gen_count, best_fitness_value):
        if self.gen_count >= gen_count:
            return
        if self.fitness_function(self.best_tree()) <= best_fitness_value:
            return
        self.next_gen()
        self.learn_until(gen_count=gen_count,best_fitness_value=best_fitness_value)

    def print(self):
        print(str(self.gen_count) + ". generation, best tree's fitness: " +
              str(self.fitness_function(self.best_tree())) +
              str(" in a population of ") +
              str(len(self.population)))

    def best_tree(self, in_population=None):
        in_population = in_population or self.population

        best_tree = in_population[0]
        best_fitness = math.inf
        for tree in in_population:
            current_fitness = self.fitness_function(tree)
            if current_fitness < best_fitness:
                best_fitness = current_fitness
                best_tree = tree
        return best_tree

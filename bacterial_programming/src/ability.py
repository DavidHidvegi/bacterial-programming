class Ability:
    """Ability of a Node"""

    STRING_CLASS_NAME = "<class 'str'>"
    FUNCTION_CLASS_NAME = "<class 'function'>"
    EXCEPTION_FUNCTION_EMPTY_ARGS = "The Ability's args_types_list is None but the function is expecting argument(s)."
    EXCEPTION_FUNCTION_ARGS_COUNT_MISMATCH = "The Ability's args attribute is not equal to the amount  of arguments " \
                                             "that your function requires"
    EXCEPTION_TYPE_ERROR = "The Ability's function's type can only be a function (if it is not a variable) or a string " \
                           "(if it is a variable)."
    EXCEPTION_EMPTY_SIGN = "The sign attribute is empty, you will not be able to draw Nodes with this ability."
    EXCEPTION_VARIABLE_WITH_ARGS = "This Ability is not a function, but the arg_types_list is not empty"

    def __init__(self, function, function_result_type, arg_types_list=None,
                 sign=None):
        self.function = function
        self.function_result_type = function_result_type
        self.arg_types_list = arg_types_list
        self.sign = sign
        self.is_variable = str(type(self.function)) == Ability.STRING_CLASS_NAME
        self._validate_self()

    def _validate_self(self):
        """Validate if the Ability was created correctly."""

        if str(type(self.function)) == Ability.FUNCTION_CLASS_NAME:
            if self.function.__code__.co_argcount > 0:
                if not self.arg_types_list:
                    raise Exception(Ability.EXCEPTION_FUNCTION_EMPTY_ARGS)
                if len(self.arg_types_list) != self.function.__code__.co_argcount:
                    raise Exception(Ability.EXCEPTION_FUNCTION_ARGS_COUNT_MISMATCH)
                if not self.sign:
                    raise Exception(Ability.EXCEPTION_EMPTY_SIGN)
        elif str(type(self.function)) == Ability.STRING_CLASS_NAME:
            if self.arg_types_list:
                raise Exception(Ability.EXCEPTION_VARIABLE_WITH_ARGS)
        else:
            raise Exception(Ability.EXCEPTION_TYPE_ERROR)

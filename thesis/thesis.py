import random
import csv
from datetime import datetime, timedelta
from bacterial_programming.src.ability import Ability
from bacterial_programming.src.bp import BacterialProgramming
from bacterial_programming.src.visualizer import TreeVisualizer


def bp_prediction():

    blood_sugar = {}
    blood_pressure_sys = {}
    blood_pressure_dia = {}
    heart_rate = {}
    with open('database.csv', newline='\n') as csv_file:
        reader = csv.DictReader(csv_file)
        for row in reader:
            date = row['Dátum']
            blood_sugar[date] = float(row['Vércukorszint'])
            blood_pressure_sys[date] = int(row['Vérnyomás SYS'])
            blood_pressure_dia[date] = int(row['Vérnyomás DIA'])
            heart_rate[date] = int(row['Pulzus'])

    learn_dates = []
    test_dates = []
    for i in range(31):
        learn_dates.append(str((datetime.strptime("2018-01-09", '%Y-%m-%d') + timedelta(days=i)).date()))
    for i in range(21):
        test_dates.append(str((datetime.strptime("2018-02-09", '%Y-%m-%d') + timedelta(days=i)).date()))

    abilities = [
        Ability(function='forecast_for_date', function_result_type="date"),
        Ability(function=lambda fd, dn: blood_sugar[
            str((datetime.strptime(fd, '%Y-%m-%d') - timedelta(days=dn)).date())
        ], function_result_type=float, arg_types_list=["date", "datenum"], sign="bloodsugar d-n"),
        Ability(function=lambda fd, dn: blood_pressure_sys[
            str((datetime.strptime(fd, '%Y-%m-%d') - timedelta(days=dn)).date())
        ], function_result_type=float, arg_types_list=["date", "datenum"], sign="bp_sys d-n"),
        Ability(function=lambda fd, dn: blood_pressure_dia[
            str((datetime.strptime(fd, '%Y-%m-%d') - timedelta(days=dn)).date())
        ], function_result_type=float, arg_types_list=["date", "datenum"], sign="bp_dia d-n"),
        Ability(function=lambda fd, dn: heart_rate[
            str((datetime.strptime(fd, '%Y-%m-%d') - timedelta(days=dn)).date())
        ], function_result_type=float, arg_types_list=["date", "datenum"], sign="heartrate d-n"),
        Ability(function=lambda: random.randint(-10, 10), function_result_type=float),
        Ability(function=lambda: random.randint(1, 8), function_result_type="datenum"),
        Ability(function=lambda x, y: x + y, function_result_type=float,
                arg_types_list=[float, float], sign="+"),
        Ability(function=lambda x, y: x * y, function_result_type=float,
                arg_types_list=[float, float], sign="*"),
        Ability(function=lambda x, y: int(x / y) if y != 0 else 999999, function_result_type=float,
                arg_types_list=[float, float], sign="/"),
    ]

    def fitness_function(t):
        fitness = 0
        for fd in learn_dates:
            tree_result = t.eval({'forecast_for_date': fd})
            if str(type(tree_result)) == "<class 'str'>":
                return 999999
            fitness = fitness + abs(tree_result - blood_pressure_sys[fd])

        fitness = fitness + 0.1 * t.node_count()
        return fitness

    bp = BacterialProgramming(abilities=abilities,
                              fitness_function=fitness_function,
                              population_size=50,
                              clone_count=10,
                              infection_count=10,
                              only_functionals_below_count=0,
                              only_terminals_above_count=30)

    for k in range(31):
        good_answers = 0
        for check_date in test_dates:
            if abs(bp.best_tree().eval({'forecast_for_date': check_date}) - blood_pressure_sys[check_date])/\
                    blood_pressure_sys[check_date] < 0.08:
                good_answers = good_answers + 1

        print("%s. generation, fitness: %s, with %s percent of the predictions are correct" % (
            bp.gen_count,
            fitness_function(bp.best_tree()),
            str(good_answers/len(test_dates))))

        image = TreeVisualizer(bp.best_tree())
        image.export_as_image(image_path='tree%s.png' % k)
        bp.learn_until(gen_count=bp.gen_count + 10, best_fitness_value=0)

    bp.best_tree().print(fitness_function=fitness_function)


bp_prediction()

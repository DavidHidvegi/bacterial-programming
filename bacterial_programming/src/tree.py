import math
import random

from bacterial_programming.src.node import Node
from bacterial_programming.src.visualizer import TreeVisualizer


class Tree:
    """A tree representation of a program in Bacterial Programming"""

    def __init__(self, abilities, only_functionals_below_count=0, only_terminals_above_count=10):
        self.abilities = abilities
        self.only_functionals_below_count = only_functionals_below_count
        self.only_terminals_above_count = only_terminals_above_count
        self.functional_abilities = [a for a in self.abilities if a.arg_types_list]
        self.terminal_abilities = [a for a in self.abilities if not a.arg_types_list]
        if only_functionals_below_count > 0:
            self.root = Node(ability=self.functional_abilities[random.randint(0, len(self.functional_abilities) - 1)])
        else:
            self.root = Node(ability=self.abilities[random.randint(0, len(self.abilities) - 1)])
        self.refill_node_with_children_recursively(self.root)
        self.nodes = {}
        self.children = {}
        self.renumber_nodes_and_children()

    def refill_node_with_children_recursively(self, node):
        if not node.ability.arg_types_list:
            return
        if node.children:
            node.children.clear()
        else:
            node.children = []

        for arg_type in node.ability.arg_types_list:
            node.children.append(self.create_child(parent_node=node, child_result_type=arg_type))

        for child in node.children:
            if child.ability.arg_types_list:
                self.refill_node_with_children_recursively(child)

    def create_child(self, parent_node, child_result_type):
        """Returns the created child under the node"""

        if self.node_count() < self.only_functionals_below_count:
            possible_abilities = [a for a in self.functional_abilities
                                  if a.function_result_type == child_result_type]
        elif self.node_count() > self.only_terminals_above_count:
            possible_abilities = [a for a in self.terminal_abilities
                                  if a.function_result_type == child_result_type]
        else:
            possible_abilities = [a for a in self.abilities if a.function_result_type == child_result_type]
        new_node_ability = possible_abilities[random.randint(0, len(possible_abilities) - 1)]

        return Node(ability=new_node_ability, parent=parent_node)

    def node_count(self):
        return Tree.node_count_from_node(self.root)

    @staticmethod
    def node_count_from_node(node):
        if node.children:
            count = 1
            for child in node.children:
                count = count + Tree.node_count_from_node(child)
            return count
        else:
            return 1

    def eval(self, variables):
        return self.root.eval(variables)

    def print(self, fitness_function=None):
        self.renumber_nodes_and_children()
        print("\n------------------------------")
        print("Printing tree with %s nodes" % self.node_count())
        if fitness_function:
            print("The tree's fitness is %s" % str(fitness_function(self)))
        print("")
        TreeVisualizer(self).draw_in_console()
        print("------------------------------")

    def renumber_nodes_and_children(self):
        self.nodes = {}
        self.children = {}
        self._renumber_nodes_and_children(node=self.root)

    def _renumber_nodes_and_children(self, node, prev_count=0):
        parent = prev_count
        self.nodes[prev_count] = node
        prev_count = prev_count + 1
        if node.children:
            children_of_node = []
            for i in range(len(node.children)):
                children_of_node.append(prev_count)
                self._renumber_nodes_and_children(node=node.children[i], prev_count=prev_count)
                prev_count = prev_count + Tree.node_count_from_node(node.children[i])
            self.children[parent] = children_of_node

    def bacterial_mutate(self, fitness_function, clone_count=3):
        self.renumber_nodes_and_children()
        for i in range(-1+self.node_count(), -1, -1):
            self._bacterial_mutate_at_node_count(self.nodes[i], fitness_function, clone_count)
        self.renumber_nodes_and_children()

    def _bacterial_mutate_at_node_count(self, node, fitness_function, clone_count):
        original_subtree_root = node.clone()
        tree_formations = {fitness_function(self): original_subtree_root}

        if original_subtree_root.ability in self.terminal_abilities:
            possible_abilities = [a for a in self.terminal_abilities
                                  if a.function_result_type == node.ability.function_result_type]
        else:
            possible_abilities = [a for a in self.abilities
                                  if a.function_result_type == node.ability.function_result_type]
        rand_max = len(possible_abilities)-1

        for _ in range(clone_count):
            new_subtree_root = Node(ability=possible_abilities[random.randint(0, rand_max)],
                                    parent=original_subtree_root.parent)
            node.become_copy_of(new_subtree_root)
            self.refill_node_with_children_recursively(node)

            tree_formations[fitness_function(self)] = node.clone()

        best_fitness = math.inf

        for fitness_value, subtree_root_node in tree_formations.items():
            if fitness_value < best_fitness:
                best_fitness = fitness_value
        best_subtree_root = tree_formations[best_fitness]

        node.become_copy_of(best_subtree_root)

    def infect_tree(self, target_tree):
        self.renumber_nodes_and_children()
        target_tree.renumber_nodes_and_children()
        base_subtree_root = self.nodes[random.randint(0, len(self.nodes)-1)]

        possible_target_nodes = []
        for key, node in target_tree.nodes.items():
            if node.ability.function_result_type == base_subtree_root.ability.function_result_type:
                possible_target_nodes.append(node)

        if possible_target_nodes:
            target_root = possible_target_nodes[random.randint(0, len(possible_target_nodes)-1)]
            Tree.copy_subtree(base_subtree_root=base_subtree_root, target_subtree_root=target_root)
            target_tree.renumber_nodes_and_children()

    @staticmethod
    def copy_subtree(base_subtree_root, target_subtree_root):

        def clone_args_recursively(root_node):
            if not root_node.ability.arg_types_list:
                return
            children = root_node.children.copy()
            root_node.children.clear()
            for child in children:
                new_child = root_node.add_child(child.clone())
                clone_args_recursively(new_child)

        target_parent = target_subtree_root.parent
        target_subtree_root.become_copy_of(node=base_subtree_root)
        target_subtree_root.parent = target_parent
        clone_args_recursively(target_subtree_root)
